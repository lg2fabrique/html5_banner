## English version is below ##

# METATAGS #
Le format se configure via ce metatag.
```
#!html
<meta name="ad.size" content="width=300,height=250">
```
La couleur de la bordure se configure via ce metatag. Pour ne pas mettre de bordure, il suffit de retirer cette ligne.
```
#!html
<meta name="ad.border.color" content="#878787">
```
Pour désactiver entièrement le "clicktag". Pour le cas quand le AdServer veut placer le clicktag sur le iframe directement.
Le clicktag de la banniàre sera active si le meta est à "true" ou s'il est complètement retiré.
```
#!html
<meta name="ad.clicktag" content="false">
```

# CLICKTAG #
Le template est prêt pour l’utilisation avec *DCM*, *DCM Rich Media(DoubleClick Studio avec Enabler.js)*, *AdGear* et *Google Display Network(Adwords)*.

* Cette dernière permet le chargement de la librairie de *DCM Rich Media*.
```
#!html
<script type="text/javascript" src="https://s0.2mdn.net/ads/studio/Enabler.js"> </script>
```

* La méthode *onClickTag(event);* permet de faire un appel au *clicktag*. Le deuxième paramètre permet d’identifier l’événement(pas obligatoire).
```
#!html
<div class="cta" onclick="javascript:onClickTag(event, 'cta')"></div>
```

# CSS #
* \#ad et #stage ne doivent pas être retirées. Il peuvent être modifiées par contre.
* .cta est un exemple d’utilisation pour faire un « call-to-action ».
* .thin pour appliquer une espace fine. <span class="thin">&nbsp;</span>

# INIT() #
La méthode *init()* est le début du code de la bannière. C'est là que vous devez commencer votre script.

# LIB.MIN.JS #
* Vous pouvez ajouter d’autres librairies dans ce fichier.
* Ne pas ajouter jQuery. Tout doit se faire en Javascript Vanilla.

# MATH.RANDOM() #
* Étant donné que certain AdServer ne permet pas d'utiliser Math.random(), la méthode RandNum() permet de faire la même chose sans passer par un Math.random().



# STAGE #
L'objet *stage* retourne:
```
#!javascript
stage.width(); // le width de la bannière
stage.height(); // le height de la bannière
stage.DOMElement(); // l'élément du stage dans le DOM
stage.pauseAllVideos() //pause tous les vidéos du DOM
```
```
#!javascript
stage.resizeTo(300,600); //Pour changer le format. Utiler pour les bannières expando.
```
# CONTEXT #
L'objet *context* retourne:
```
#!javascript
context.getType(); // retourne le type de server publicitaire
Context.isTouch(); //est-ce un ecran touch
context.isMobile(); //est-ce un mobile ?
context.isTablet(); //est-ce une tablette ?
context.isDesktop(); //est-ce un ordinateur ?
context.isIE('9'); //est-ce une version spécifique de IE ?
context.isChrome(); //est-ce Chrome ?
context.isFirefox(); //est-ce Firefox ? 
context.isSafariMobile(); //est-ce Safari sur mobile ? 
context.isSafari(); //est-ce Safari sur ordinateur ?
context.isAndroid(); //est-ce la plateforme Android ?
context.isIOS(); //est-ce la plateforme iOs ?
context.isIPad(); //est-ce un iPad ?
context.isIPhone(); //est-ce un iPhone ?
context.getIEVersion(); //quel version de IE
context.getParameterByName('param'); //retourne la valeur d'un querystring

Context.ADSERVERS //servers publicitaire supportés
```
  
# INFORMATIONS SUPPLÉMENTAIRES #
* L’utilisation de TimelineLite est fortement recommandée. Par sa simplicité d’utilisation.
* Ne pas utiliser les webfonts sauf si c'est spécifié (aka Google Font).
* Utilisez des svg le plus possible.
* La création de SVG se fait via Illustrator en s’assurant de bien « outliner » le vectorielle.
* L'outils SVGO permet de bien optimiser les svg. S'exécute sur NodeJs.
* https://tinypng.com/ permet d'optimiser vos PNG.
* Si vous utilisez des images, pensez à les faire 2x plus grosses pour le support rétina.
* N'utilisez pas les keyframe en CSS3.
* Ne laisser aucun fichier inutilisé dans le dossier de la bannière.
* Ne laisser aucun appel de fichier inexistant/inutilisé en commentaire. Ex: un background-image en css. 
* Le poids dans les specs est le poids du "package" zippé.
* Le fichier index.html ne doit pas dépasser 50ko sauf si spécifié.
* Testez toujours les bannières en *desktop*, *mobile* et *tablette*

# Plateformes a supporté (donc les bannières doivent fonctionner sur...) #
* Chrome
* Firefox
* Internet Explorer 9-10-11
* Safari
* iOs 7-8-9-10 (iPhone et tablette)
* Android 4.4 - 5 - 6 - 7









# METATAGS #
Set up the size with this metatag.
```
#!html
<meta name="ad.size" content="width=300,height=250">
```

Set up the border color with this metatag. Remove this line for ads with no border.
```
#!html
<meta name="ad.border.color" content="#878787">
```

# CLICKTAG #
This template is ready for *DCM*, *DCM Rich Media(DoubleClick Studio with Enabler.js)*, *AdGear* et *Google Display Network(Adwords)*

* This one load *DCM Rich Media* librairies.
```
#!html
<script type="text/javascript" src="https://s0.2mdn.net/ads/studio/Enabler.js"> </script>
```

* The function *onClickTag(event);* call the clicktag. The second parameter setup an event name (not mandatory).
```
#!html
<div class="cta" onclick="javascript:onClickTag(event, 'cta')"></div>
```

# CSS #
* \#ad and #stage didn't be removed, but can be modified.
* .cta is a example to do a « call-to-action ».
* .thin to apply a thin space. <span class="thin">&nbsp;</span>

# LIB.MIN.JS #
* You can add other librairies in this file.
* Don't add jQuery, all Javascript must be Vanilla.


# MATH.RANDOM() #
* Some AdServer does not allow the use of Math.random(). This template have a function RandNum(). This one do the same thing, but without Math.random().

# INIT() #
This function is the beginning. This is where you must start your code.


# STAGE #
```
#!javascript
stage.width(); // banner width
stage.height(); // banner height
stage.DOMElement(); // stage DOM element
stage.pauseAllVideos() //pause all videos
```
```
#!javascript
stage.resizeTo(300,600); //To change the banner size. Use it for expanding banner.
```
# CONTEXT #
```
#!javascript
context.getType(); // get the server type
Context.isTouch(); //is it touchscreen ?
context.isMobile(); //is it mobile ?
context.isTablet(); //is it tablette ?
context.isDesktop(); //is it desktop ?
context.isIE('9'); //is it IE ?
context.isChrome(); //is it Chrome ?
context.isFirefox(); //is it Firefox ? 
context.isSafariMobile(); //is it Safari on mobile ? 
context.isSafari(); //is it Safari on desktop ?
context.isAndroid(); //is it Android ?
context.isIOS(); //is it iOs ?
context.isIPad(); //is it iPad ?
context.isIPhone(); //is it iPhone ?
context.getIEVersion(); //which is the IE version
context.getParameterByName('param'); //get querystring value

Context.ADSERVERS //list of supported AdServer
```

# ADDITIONAL INFORMATION #
* Using TimelineLite is highly recommended. 
* Don't use WebFonts unless it is specified.
* Use svg as possible.
* The tool SVGO can optimizes SVG. It's works with NodeJs.
* https://tinypng.com/ can optimizes PNG.
* If you use images, think about retina. Don't forget to use 2x images.
* Don't use keyframe in CSS3.
* Don't leave any unused file in the banner folder.
* The spec weight is the weight of zipped package.
* The index.html file should not exceed 50kb unless specified.
* Always test the banners: *desktop*, *mobile* and *tablet*

# Supported platforms (So the banners must work on)#
* Chrome
* Firefox
* Internet Explorer 9-10-11
* Safari
* iOs 7-8-9-10 (iPhone and tablet)
* Android 4.4 - 5 - 6 - 7