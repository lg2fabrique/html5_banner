RandNum = (function() {
    var seed = new Date().getTime();
    return function() {
        seed = ((seed + 0x7ED55D16) + (seed << 12)) & 0xFFFFFFFF;
        seed = ((seed ^ 0xC761C23C) ^ (seed >>> 19)) & 0xFFFFFFFF;
        seed = ((seed + 0x165667B1) + (seed << 5)) & 0xFFFFFFFF;
        seed = ((seed + 0xD3A2646C) ^ (seed << 9)) & 0xFFFFFFFF;
        seed = ((seed + 0xFD704C5) + (seed << 3)) & 0xFFFFFFFF;
        seed = ((seed ^ 0xB55A4F09) ^ (seed >>> 16)) & 0xFFFFFFFF;
        return (seed & 0xFFFFFFF) / 0x10000000;
    };
}());

function openClickTag(event, type, url){
    if(Context.getType() == Context.ADSERVERS.NONE) return;

    event.preventDefault();
    event.stopPropagation();
    stage.pauseAllVideos();

    if(url && Context.getType() !== Context.ADSERVERS.ADWORDS && Context.getType() !== Context.ADSERVERS.STANDARD){
        console.log('clickTag overrided by : ' + url);
    } else url = null;

    if(Context.getType() === Context.ADSERVERS.STANDARD){ //Google Display Network and DoubleClick Standard clicktag
        window.open(window.clickTag, '_blank');
    } else if(Context.getType() === Context.ADSERVERS.ADGEAR) { //AdGear clicktag
        ADGEAR.html5.clickThrough('clickTAG', url || null);
    } else if(Context.getType() === Context.ADSERVERS.DCM_RICH){ //DoubleClick Studio clicktag
        if(url) Enabler.exitOverride("clicktag", url);
        else Enabler.exit('clicktag');
    } else if(Context.getType() === Context.ADSERVERS.ADWORDS){
        ExitApi.exit(); // Google AdWords (new clicktag)
    } else {
        console.log('*** Unknown adserver ***');
        window.open(window.clickTag, '_blank');
    }
}

(function() {
    var videos = document.querySelectorAll('video');
    for(var i = 0; i < videos.length; i++){
        videos[i].addEventListener('ended', function(){
            document.webkitExitFullscreen && document.webkitExitFullscreen();
            document.mozCancelFullscreen && document.mozCancelFullscreen();
            document.exitFullscreen && document.exitFullscreen();
        }, false);
    }
}());

(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

(function() {

    var instance;
    
    function Context() {
        
        if(!instance) instance = this;
        else return instance;

        var self = this;

        self.initialize = function(obj) {
            for(var i in obj) {
                self[i] = obj[i];
                Context[i] = obj[i];
            }
            
            for(var i in Context){
                self[i] = Context[i];
            }
        };

        self.toString = function(){ return '[object Context]';};

        self.initialize.apply(self, arguments);
        return self;
    }

    Context.getInstance = (function() {
        return function(option) {
            if(!instance) return new Context(option);
            else return instance;
        };
    })();
    
    Context.isMobile = (function() {
        return function() {
            if(typeof window.orientation !== 'undefined' || Context.isTouch()) return true;
            else return false;
        };
    })();
    Context.isTablet = (function() {
        return function() {
            if(Context.isIPad()) return true;
            else if(Context.isMobile() && Context.getViewport().width > 767) return true;
            else return false;
        };
    })();
    Context.isDesktop = (function() {
        return function() {
            if(typeof window.orientation !== 'undefined') return false;
            else return true;
        };
    })();
    Context.isTouch = (function() {
        return function() {
            return !!('ontouchstart' in window);
        };
    })();

    Context.isIE = (function() {
        return function(version) {
            if(version!== undefined){
                return Context.getIEVersion() === version ? true : false;
            }else return navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0;
        };
    })();
    Context.isChrome = (function() {
        return function() {
            var ua = navigator.userAgent;
            if ( /Chrome/i.test( ua ) ) return true;
            else return false;
        };
    })();
    Context.isFirefox = (function() {
        return function() {
            var ua = navigator.userAgent;
            if (/Firefox/i.test( ua )) return true;
            else return false;
        };
    })();
    Context.isSafariMobile = (function() {
        return function() {
            var ua = navigator.userAgent;
            if (/Mobile(\/.*)? Safari/i.test( ua )) return true;
            else return false;
        };
    })();
    Context.isSafari = (function() {
        return function() {
            var ua = navigator.userAgent;
            if(!Context.isSafariMobile()){
                if (/Safari/i.test( ua )){
                    if (/Chrome/i.test( ua )) return false;
                    else return true;
                }else return false;
            }else return false;
        };
    })();
    Context.isAndroid = (function() {
        return function() {
            var ua = navigator.userAgent;
            if ( /Android/i.test( ua ) ) return true;
            else return false;
        };
    })();
    Context.isIOS = (function() {
        return function() {
            var ua = navigator.userAgent;
            if (/iP[ao]d|iPhone/i.test( ua )) return true;
            else return false;
        };
    })();
    Context.isIPad = (function() {
        return function() {
            var ua = navigator.userAgent;
            if (/iP[ao]d/i.test( ua )) return true;
            else return false;
        };
    })();
    Context.isIPhone = (function() {
        return function() {
            var ua = navigator.userAgent;
            if (/iPhone/i.test( ua )) return true;
            else return false;
        };
    })();
    Context.getIEVersion = (function() {
        return function() {
            var version = false;
            var ieold = (/MSIE (\d+\.\d+);/.test(navigator.userAgent));
            var trident = !!navigator.userAgent.match(/Trident\/7.0/);
            var rv = navigator.userAgent.indexOf("rv:11.0");

            if (ieold) version = Number(RegExp.$1);
            if (navigator.appVersion.indexOf("MSIE 10") != -1) version = 10;
            if (trident && rv != -1) version = 11;
            
            return version;
        };
    })();
    Context.getValue = (function() {
        return function(name, defaultValue) {
            if(!instance) throw new Error('Context is not defined');
            else{
                if(instance[name]) return instance[name];
                else{
                    if(defaultValue !== undefined) return defaultValue;
                    else return false;
                }
            }
        };
    })();
    Context.getParameterByName = function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    
    Context.ADSERVERS = {
        NONE:'none',
        STANDARD:'Google Display Network or DoubleClick Standard',
        ADWORDS:'Google AdWords',
        DCM_RICH:'DoubleClick Studio',
        ADGEAR:'AdGear'
    }
    
    var type = Context.ADSERVERS.STANDARD;
    Context.setType = function(server){
        type = server;
    };
    Context.getType = function(){
        return type;
    };
    
    window.Context = Context;

}());

(function() {
        
    var VERSION = '1.0.15';
    
    function Stage() {

        var self = this;
        var metas = getMetas();
        var size = setSize();
        
        self.initialize = function() {
            setCSS();
            setBorder();

            if((metas['ad.clicktag'] && metas['ad.clicktag'] === 'true') || !metas['ad.clicktag']){
                if(window.Enabler) Context.setType(Context.ADSERVERS.DCM_RICH);
                else if(window.ExitApi) Context.setType(Context.ADSERVERS.ADWORDS);
                else if(window.ADGEAR) Context.setType(Context.ADSERVERS.ADGEAR);
            }else{
                window.Enabler = null;
                window.ExitApi = null;
                window.ADGEAR = null;
                Context.setType(Context.ADSERVERS.NONE);
            }
            
            window.onload = function(){
                if(Context.getType() === Context.ADSERVERS.DCM_RICH){
                    if (Enabler.isInitialized()) initialized();
                    else Enabler.addEventListener(studio.events.StudioEvent.INIT, initialized);
                } else ready();
            };
        };

        self.width = function(){ 
            return size.width;
        };
        self.height = function(){ 
            return size.height; 
        };
        self.DOMElement = function(){
            return document.getElementById('stage');
        };

        self.resizeTo = function(width, height){
            setSize(width, height);
            setBorder(width, height);
        };
        
        self.pauseAllVideos = function() {
            var videos = document.querySelectorAll('video');
            for(var i = 0; i < videos.length; i++){
                if(!videos[i].paused) videos[i].pause();
            }
        };

        self.addChild = function(child){
            var e = document.createElement('div');
            e.innerHTML = child;
            while(e.firstChild) self.DOMElement().appendChild(e.firstChild);
        };
        self.removeChild = function(child){
            self.DOMElement().parentNode.removeChild(child);
            return false;
        };

        function initialized(){
            if(Context.getType() === Context.ADSERVERS.DCM_RICH){
                
                var videos = document.querySelectorAll('video');
                if(videos){
                    Enabler.loadModule(studio.module.ModuleId.VIDEO, function() {
                        for(var i = 0; i < videos.length; i++){
                            studio.video.Reporter.attach('video'+i, videos[i]);
                        }
                    });
                }
                
                if (Enabler.isPageLoaded) ready();
                else Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, ready);
            } else ready();
        }
        function ready(){
            console.log('version -> ' + VERSION);
            
            if(Context.getType() == Context.ADSERVERS.NONE){
                console.info('*** clicktag is deactivated -> AdServer can add clicktag all over the ad ***');
                console.info('*** Ready for Google AdWords(or other AdServer) without clicktag ***');
            }else{
                console.info('*** Ready for ' + Context.getType() +' ***');
            }
            
            init();
            if(Context.isIOS()) setTimeout(function(){document.getElementById('ad').style.visibility = 'visible';}, 100)
            else document.getElementById('ad').style.visibility = 'visible';
        }

        function getMetas(){
            var metas = {};
            var metastag = document.getElementsByTagName('meta');
            for (i = 0; i < metastag.length; i++) { 
                metas[metastag[i].getAttribute('name')] = metastag[i].getAttribute('content');
            } 
            return metas;
        }
        
        function setSize(width, height){
            if (metas['ad.size'] || (width && height)) { 
                var size = {};
                
                if(!width && !height){
                    var values = metas['ad.size'].split(',');
                    for(var i in values){
                        var prop = values[i].split('=');
                        size[prop[0]] = prop[1];
                    }
                }else{
                    size.width = width;
                    size.height = height;
                }

                document.getElementById('ad').style.width = size.width + 'px';
                document.getElementById('ad').style.height = size.height + 'px';
                
                return size; 
            }else{
                throw new Error('meta tag "ad.size" is not defined');
            } 
        }
        function setBorder(width, height){
            if(metas['ad.border.color']){
                if(!document.querySelector('.borders')){
                    var head = document.head || document.getElementsByTagName('head')[0];
                    var css = '.border {position: absolute;top:0; left:0;width:1px; height:1px;z-index: 9999999; background-color:#000000}';
                    var div = document.createElement("div");
                    div.innerHTML = '<p>x</p><style>' + css + '</style>';
                    head.appendChild(div.childNodes[1]);

                    self.addChild('<div class="borders">\n'+
                        '<div class="border border-top"></div>\n'+
                        '<div class="border border-right"></div>\n'+
                        '<div class="border border-bottom"></div>\n'+
                        '<div class="border border-left"></div>\n'+
                    '</div>');
                }
                
                var borders = document.querySelectorAll('.border');
                for(var i = 0; i < borders.length; i++) borders[i].style.backgroundColor = metas['ad.border.color'];
                
                if(width == undefined) width = self.width();
                if(height == undefined) height = self.height();
                
                document.querySelector('.border-top').style.width = width + 'px';
                document.querySelector('.border-right').style.height = height + 'px';
                document.querySelector('.border-right').style.left = (width - 1) + 'px';
                document.querySelector('.border-bottom').style.width = width + 'px';
                document.querySelector('.border-bottom').style.top = (height - 1) + 'px';
                document.querySelector('.border-left').style.height = height + 'px';
            }
        }
        function setCSS(){
            var head = document.head || document.getElementsByTagName('head')[0];
            var css = '*{padding:0px;margin:0px;}#ad, #stage{-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;}#ad{visibility:hidden;}#ad > a { visibility: hidden }#stage{position: relative;width: 100%; height: 100%;background-color: #ffffff;cursor: pointer;overflow: hidden;}.thin { letter-spacing: -0.2em; }';
            var div = document.createElement("div");
            div.innerHTML = '<p>x</p><style>' + css + '</style>';
            head.appendChild(div.childNodes[1]);
        }
        self.initialize.apply(self, arguments);
        return self;
    }

    window.Stage = Stage;

}());

var context = new Context();
var stage = new Stage();